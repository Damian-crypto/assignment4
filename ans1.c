#include <stdio.h>

int main()
{
	int sum = 0, temp = 0;

	while (1)
	{
		printf("Enter an integer: ");
		scanf("%d", &temp);

		if (temp > 0)
		{
			sum += temp;
		}
		else
		{
			break;
		}
	}

	printf("Sum of all positive integers your entered: %d\n", sum);

	return 0;
}
