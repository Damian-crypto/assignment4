#include <stdio.h>

int main()
{
	int n = 0, hasFactors = 0;
	printf("Enter a number to check prime: ");
	scanf("%d", &n);

	if (n == 0 || n == 1)
	{
		hasFactors = 1;
	}
	else if (n < 0)
	{
		printf("Negative numbers cannot be primes.");

		return 0;
	}
	else
	{
		for (int i = 2; i <= n / 2; i++)
		{
			if (n % i == 0)
			{
				hasFactors = 1;
				break;
			}
		}
	}

	if (hasFactors)
	{
		printf("The number you entered is not a prime.");
	}
	else
	{
		printf("The number you entered is a prime.");
	}

	return 0;
}
