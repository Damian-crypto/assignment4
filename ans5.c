#include <stdio.h>

int main()
{
	int n = 0;
	printf("Enter a number to print mulplication tables from 1 to n: ");
	scanf("%d", &n);

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			printf("%d x %d = %d\n", i, j, i * j);
		}
		printf("\n");
	}

	return 0;
}
