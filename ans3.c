#include <stdio.h>

int main()
{
	int number = 0, remainder = 0;

	printf("Enter a number to reverse: ");
	scanf("%d", &number);

	while (number != 0)
	{
		remainder *= 10;
		remainder += number % 10;
		number /= 10;
	}

	printf("The number you entered in reversed order: %d\n", remainder);

	return 0;
}
