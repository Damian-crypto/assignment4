#include <stdio.h>

int main()
{
	int number = 0, negativity = 0;
	printf("Enter a number to find factors: ");
	scanf("%d", &number);

	if (number < 0)
	{
		negativity = 1;
		number = -number; // to get absolute value
	}

	for (int i = 1; i <= number; i++)
	{
		if (number % i == 0)
		{
			if (negativity)
			{
				printf("%d is a factor of %d.\n", -i, number);
			}
			printf("%d is a factor of %d.\n", i, number);
		}
	}

	return 0;
}
